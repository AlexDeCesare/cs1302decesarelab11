package view;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 * This class handles the drawing for the pane 
 * 
 * @author Alex DeCesare
 * @version 08-July-2020
 */

public class DrawPane extends Pane {

	/**
	 * The constructor for the draw pane
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	
	public DrawPane() {
		
		super();
		super.setPrefSize(600, 400);
	
		this.drawChair();
		this.drawPerson();
		this.drawDesk();
		this.drawComputer();
		this.drawText();
	}
	
	private void drawChair() {
		
		Rectangle firstChairLeg = new Rectangle(0, 250, 20, 100);
		firstChairLeg.xProperty().bind(super.widthProperty().divide(2).subtract(250));
		firstChairLeg.setFill(Color.SADDLEBROWN);
		super.getChildren().add(firstChairLeg);
		
		Rectangle secondChairLeg = new Rectangle(0, 250, 20, 100);
		secondChairLeg.xProperty().bind(super.widthProperty().divide(2).subtract(150));
		secondChairLeg.setFill(Color.SADDLEBROWN);
		super.getChildren().add(secondChairLeg);
		
		Rectangle chairSeat = new Rectangle(0, 230, 120, 20);
		chairSeat.xProperty().bind(super.widthProperty().divide(2).subtract(250));
		chairSeat.setFill(Color.SADDLEBROWN);
		super.getChildren().add(chairSeat);
		
		Rectangle chairBack = new Rectangle(0, 70, 20, 160);
		chairBack.xProperty().bind(super.widthProperty().divide(2).subtract(250));
		chairBack.setFill(Color.SADDLEBROWN);
		super.getChildren().add(chairBack);
	}
	
	private void drawPerson() {
		
		Rectangle personBody = new Rectangle(0, 110, 70, 120);
		personBody.xProperty().bind(super.widthProperty().divide(2).subtract(220));
		personBody.setFill(Color.DARKGREEN);
		super.getChildren().add(personBody);
		
		Circle personHead = new Circle(0, 70, 40);
		personHead.centerXProperty().bind(super.widthProperty().divide(2).subtract(185));
		personHead.setFill(Color.TAN);
		super.getChildren().add(personHead);
		
	}
	
	private void drawDesk() {
		
		Rectangle firstDeskLeg = new Rectangle(250, 170, 20, 180);
		firstDeskLeg.xProperty().bind(super.widthProperty().divide(2).subtract(50));
		super.getChildren().add(firstDeskLeg);		
		
		Rectangle secondDeskLeg = new Rectangle(400, 170, 20, 180);
		secondDeskLeg.xProperty().bind(super.widthProperty().divide(2).add(100));
		super.getChildren().add(secondDeskLeg);	
		
		Rectangle deskTop = new Rectangle(250, 170, 150, 20);
		deskTop.xProperty().bind(super.widthProperty().divide(2).subtract(50));
		super.getChildren().add(deskTop);
	}
	
	private void drawComputer() {
		
		Rectangle computerBase = new Rectangle(325, 150, 20, 20);
		computerBase.xProperty().bind(super.widthProperty().divide(2).add(20));
		computerBase.setFill(Color.DARKGRAY);
		super.getChildren().add(computerBase);
		
		Rectangle computerMonitor = new Rectangle(315, 50, 40, 100);
		computerMonitor.xProperty().bind(super.widthProperty().divide(2).add(10));
		computerMonitor.setFill(Color.DARKGRAY);
		super.getChildren().add(computerMonitor);
		
		Rectangle computerScreen = new Rectangle(315, 50, 2, 100);
		computerScreen.xProperty().bind(super.widthProperty().divide(2).add(8));
		computerScreen.setFill(Color.BLUE);
		super.getChildren().add(computerScreen);
		
	}
	
	private void drawText() {
		
		String theTitleText = "Just Compile" + System.lineSeparator() + "By Alex DeCesare";
		
		Text titleBox = new Text(50, 50, theTitleText);
		titleBox.xProperty().bind(super.widthProperty().subtract(120));
		super.getChildren().add(titleBox);
		
	}
	
}
