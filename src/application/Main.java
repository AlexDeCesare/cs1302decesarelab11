package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import view.DrawPane;
import javafx.scene.Scene;

/**
 * The main method for the application
 * 
 * @author Alex DeCeare
 * @version 08-July-2020
 */

public class Main extends Application {
	
	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Alex DeCesare Lab 11");
		
		DrawPane root = new DrawPane();
		Scene rootScene = new Scene(root);
		
		primaryStage.setScene(rootScene);
		
		primaryStage.show();
	}
	
	/**
	 * The main method for the project
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param args the arguments for the main method
	 */
	
	public static void main(String[] args) {
		launch(args);
	}
}
